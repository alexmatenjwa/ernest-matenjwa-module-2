void main() {
  //Array that store List of all winning since 2012
  List winningApps = [
    'Ambani Afri',
    'EasyEquities',
    'Naked Insurance',
    'Khula', //2018 is in index 3 starting from 0
    'Shyft', //2017 is in index 4 starting from 0
    'Domestly',
    'Wumdrop',
    'Live Inspect',
    'SnapScan',
    'FNB'
  ];
  //getting winning app in 2017 and 2018 by index number
  var winningApp2017 = winningApps[4];
  var winningApp2018 = winningApps[3];

  //get total number of apps
  int totalNoApps = winningApps.length;

  //Loop through the Array
  print('All winning apps from 2012(Sorted)');
  print('**********************************');
  for (int i = 0; i < totalNoApps; i++) {
    //sort list
    winningApps.sort();

    // print sorted list
    print(winningApps[i]);
  }
  //print winning app of 2017 and 2018
  print('Winning apps in 2017 and 2018');
  print('**********************************');
  print('Winning app 2017: $winningApp2017');
  print('Winning app 2018: $winningApp2018');
  print('**********************************');
  //c) Print Total number of apps
  print('Total number of apps since 2012: $totalNoApps');
}
