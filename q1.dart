void main() {
  //Assign my data to varables
  String name = 'Ernest',
      surname = 'Matenjwa',
      favApp = 'tikTok',
      city = 'Nelspruit';

  //print the data
  print('My Details(name, surname, fav app and city)');
  print('*******************************************');
  print('Name \t\t:' + name);
  print('Surname \t:' + surname);
  print('Favourate app \t:' + favApp);
  print('City \t\t:' + city);
}
