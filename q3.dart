void main() {
  //create an opject
  var app = new App();

  //Array of all winning apps from 2012
  List winningApps = [
    'Ambani Afri',
    'EasyEquities',
    'Naked Insurance',
    'Khula',
    'Shyft',
    'Domestly',
    'Wumdrop',
    'Live Inspect',
    'SnapScan',
    'FNB'
  ];

  for (int i = 0; i < winningApps.length; i++) {
    //create the if statement to allow assigning details using
    //item or app index from the array
    if (i == 0) {
      //convert name to upper case
      app.appName = app.nameToUpperCase(winningApps[0]);
      app.category = 'Learning';
      app.developer = 'Ambani Africa';
      app.year = 2021;

      var appName = app.appName;
      var category = app.category;
      var developer = app.developer;
      var year = app.year;

      print('App Details');
      print('***************************************');
      print('App name \t: $appName');
      print('Category \t: $category');
      print('Developer \t: $developer');
      print('Year \t \t: $year');
    }

    if (i == 1) {
      //convert name to upper case
      app.appName = app.nameToUpperCase(winningApps[1]);
      app.category = 'Trade';
      app.developer = 'First world trader';
      app.year = 2020;

      var appName = app.appName;
      var category = app.category;
      var developer = app.developer;
      var year = app.year;

      print('App Details');
      print('***************************************');
      print('App name \t: $appName');
      print('Category \t: $category');
      print('Developer \t: $developer');
      print('Year \t \t: $year');
    }
    if (i == 2) {
      //convert name to upper case
      app.appName = app.nameToUpperCase(winningApps[2]);
      app.category = 'Claims';
      app.developer = 'Naked financial technology';
      app.year = 2019;

      var appName = app.appName;
      var category = app.category;
      var developer = app.developer;
      var year = app.year;

      print('App Details');
      print('***************************************');
      print('App name \t: $appName');
      print('Category \t: $category');
      print('Developer \t: $developer');
      print('Year \t \t: $year');
    }
    if (i == 3) {
      //convert name to upper case
      app.appName = app.nameToUpperCase(winningApps[3]);
      app.category = 'Farming';
      app.developer = 'Khula';
      app.year = 2018;

      var appName = app.appName;
      var category = app.category;
      var developer = app.developer;
      var year = app.year;

      print('App Details');
      print('***************************************');
      print('App name \t: $appName');
      print('Category \t: $category');
      print('Developer \t: $developer');
      print('Year \t \t: $year');
    }
    if (i == 4) {
      //convert name to upper case
      app.appName = app.nameToUpperCase(winningApps[4]);
      app.category = 'Digital banking';
      app.developer = 'Standard bank';
      app.year = 2017;

      var appName = app.appName;
      var category = app.category;
      var developer = app.developer;
      var year = app.year;

      print('App Details');
      print('***************************************');
      print('App name \t: $appName');
      print('Category \t: $category');
      print('Developer \t: $developer');
      print('Year \t \t: $year');
    }
    if (i == 5) {
      //convert name to upper case
      app.appName = app.nameToUpperCase(winningApps[5]);
      app.category = 'Social';
      app.developer = 'Domestly';
      app.year = 2016;

      var appName = app.appName;
      var category = app.category;
      var developer = app.developer;
      var year = app.year;

      print('App Details');
      print('***************************************');
      print('App name \t: $appName');
      print('Category \t: $category');
      print('Developer \t: $developer');
      print('Year \t \t: $year');
    }
    if (i == 6) {
      //convert name to upper case
      app.appName = app.nameToUpperCase(winningApps[6]);
      app.category = 'Social app';
      app.developer = 'Wumdrop';
      app.year = 2015;

      var appName = app.appName;
      var category = app.category;
      var developer = app.developer;
      var year = app.year;

      print('App Details');
      print('***************************************');
      print('App name \t: $appName');
      print('Category \t: $category');
      print('Developer \t: $developer');
      print('Year \t \t: $year');
    }
    if (i == 7) {
      //convert name to upper case
      app.appName = app.nameToUpperCase(winningApps[7]);
      app.category = 'Property inspection';
      app.developer = 'Radweb';
      app.year = 2014;

      var appName = app.appName;
      var category = app.category;
      var developer = app.developer;
      var year = app.year;

      print('App Details');
      print('***************************************');
      print('App name \t: $appName');
      print('Category \t: $category');
      print('Developer \t: $developer');
      print('Year \t \t: $year');
    }
    if (i == 8) {
      //convert name to upper case
      app.appName = app.nameToUpperCase(winningApps[8]);
      app.category = 'Digital banking';
      app.developer = 'FireID Payments';
      app.year = 2013;

      var appName = app.appName;
      var category = app.category;
      var developer = app.developer;
      var year = app.year;

      print('App Details');
      print('***************************************');
      print('App name \t: $appName');
      print('Category \t: $category');
      print('Developer \t: $developer');
      print('Year \t \t: $year');
    }
    if (i == 9) {
      //convert name to upper case
      app.appName = app.nameToUpperCase(winningApps[9]);
      app.category = 'Digital banking';
      app.developer = 'FirstRand Limited';
      app.year = 2012;

      var appName = app.appName;
      var category = app.category;
      var developer = app.developer;
      var year = app.year;

      print('App Details');
      print('***************************************');
      print('App name \t: $appName');
      print('Category \t: $category');
      print('Developer \t: $developer');
      print('Year \t \t: $year');
    }
  }
  //Print app details

  //app.printAppDetails();
}

//my class
class App {
  String? appName;
  String? category;
  String? developer;
  int? year;

  //function to convert name to Uppercase
  String nameToUpperCase(var name) {
    return name.toUpperCase();
  }

//function to print app details
/*  void printAppDetails() {
    //convert name to upper using a function
    var nameConvertedToUpper = nameToUpperCase(appName);
    print('App Details');
    print('***************************************');
    print('App name: \t $nameConvertedToUpper');
    print('Category: \t $category');
    print('Developer: \t $developer');
    print('Year: \t \t $year');
  }*/

}
